μNaCl ("microsalt") is an implementation of Curve25519 for the 8-bit AVR ATmega, TI MSP430 and ARM Cortex-M0 microcontroller platforms, created by Michael Hutter and Peter Schwabe, and release as Public Domain code at https://munacl.cryptojedi.org.

This is my personal repository of the code from that project, with minor modifications to facilitate a successful build on my system.

Feel free to pull from this repository is you wish and ensure you pay attention to the original licensing of the code.