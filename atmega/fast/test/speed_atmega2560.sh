#!/bin/sh

# Original script was for genuine Arduino Mega board, some changes required for cheap Chinese clone

#DEVICE=/dev/ttyACM0
DEVICE=/dev/ttyUSB0
DIR=`dirname $0`

#avrdude -cstk500v2 -p atmega2560 -P $DEVICE -b 115200 -U flash:w:$DIR/speed.hex -v
avrdude -cwiring -p atmega2560 -P $DEVICE -b 115200 -D -U flash:w:$DIR/speed.hex -v
stty -F $DEVICE raw icanon eof \^d 38400
cat < $DEVICE
