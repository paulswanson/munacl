/*
 * File:    fe_mul.S
 * Author:  Gesine Hinterwälder
 *          modifications for gcc by Peter Schwabe
 * Public Domain
 */

#include "msp430fr5969.h"  

.global fe_mul
.type fe_mul, @function
   
    
#ifdef TACCTL2_
  #define CCTLX TACCTL2
  #define CCRX TACCR2
#else
  #define CCTLX TACCTL0
  #define CCRX TACCR0
#endif

      
fe_mul:

    dint
    nop

    pushm.w     #8,R11
    push.w      R15
    sub.w       #82,R1

    bis         #MPYDLYWRTEN,&MPY32CTL0
    bic         #MPYDLY32,&MPY32CTL0
      
    ; R14 holds f
    ; R13 holds g
    ; R1 (0 ... 64) hold result
    ; R1 (66 ... 96) holds reversed g
    
    mov         @R13+,R4
    mov         @R13+,R5
    mov         @R13+,R6
    mov         @R13+,R7
    mov         @R13+,R8
    mov         @R13+,R9
    mov         @R13+,R10
    mov         @R13+,80(R1)
    mov         @R13+,76(R1)
    mov         @R13+,78(R1)
    mov         @R13+,72(R1)
    mov         @R13+,74(R1)
    mov         @R13+,68(R1)
    mov         @R13+,70(R1)
    mov         @R13+,64(R1)
    mov         @R13+,66(R1)
    
    mov         #RES0,R12
    mov         R1,R13
    add         #80,R13
    mov         #SUMEXT,R11

    mov         @R14+,&MPY32L
    mov         @R14+,&MPY32H
    mov         R4,&OP2L  
    mov         R5,&OP2H 
    sub         #4,R14
    mov         @R12+,0(R1)
    mov         @R12+,2(R1)
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    clr         &RES2
    clr         &RES3
        
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         R6,&OP2L  
    mov         R7,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         R4,&OP2L  
    mov         R5,&OP2H 
    sub         #8,R12
    sub         #8,R14
    mov         @R12+,4(R1)
    mov         @R12+,6(R1)
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3
        
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         R8,&OP2L  
    mov         R9,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         R6,&OP2L  
    mov         R7,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R4,&OP2L  
    mov         R5,&OP2H 
    sub         #8,R12
    sub         #12,R14
    mov         @R12+,8(R1)
    mov         @R12+,10(R1)
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3
    
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         R10,&OP2L  
    mov         @R13,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         R8,&OP2L  
    mov         R9,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R6,&OP2L  
    mov         R7,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R4,&OP2L  
    mov         R5,&OP2H 
    sub         #8,R12
    sub         #16,R14
    sub         #4,R13
    mov         @R12+,12(R1)
    mov         @R12+,14(R1)
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3
    
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         R10,&OP2L  
    mov         @R13,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R8,&OP2L  
    mov         R9,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R6,&OP2L  
    mov         R7,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R4,&OP2L  
    mov         R5,&OP2H 
    sub         #8,R12
    sub         #20,R14
    sub         #8,R13
    mov         @R12+,16(R1)
    mov         @R12+,18(R1)
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3
    
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R10,&OP2L  
    mov         @R13,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R8,&OP2L  
    mov         R9,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R6,&OP2L  
    mov         R7,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R4,&OP2L  
    mov         R5,&OP2H 
    sub         #8,R12
    sub         #24,R14
    sub         #12,R13
    mov         @R12+,20(R1)
    mov         @R12+,22(R1)
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3
    
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R10,&OP2L  
    mov         @R13,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R8,&OP2L  
    mov         R9,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R6,&OP2L  
    mov         R7,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R4,&OP2L  
    mov         R5,&OP2H 
    sub         #8,R12
    sub         #28,R14
    sub         #16,R13
    mov         @R12+,24(R1)
    mov         @R12+,26(R1)
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3
    
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R10,&OP2L  
    mov         @R13,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R8,&OP2L  
    mov         R9,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R6,&OP2L  
    mov         R7,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R4,&OP2L  
    mov         R5,&OP2H 
    sub         #8,R12
    sub         #28,R14
    sub         #16,R13
    mov         @R12+,28(R1)
    mov         @R12+,30(R1)
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3
    
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R13+,R4
    mov         R4,&OP2L  
    mov         @R13+,R5
    mov         R5,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R10,&OP2L  
    mov         @R13,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R8,&OP2L  
    mov         R9,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R6,&OP2L  
    mov         R7,&OP2H 
    sub         #8,R12
    sub         #24,R14
    sub         #12,R13
    mov         @R12+,32(R1)
    mov         @R12+,34(R1)
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3
    
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         R4,&OP2L  
    mov         R5,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         @R13+,R6
    mov         R6,&OP2L  
    mov         @R13+,R7
    mov         R7,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R10,&OP2L  
    mov         @R13,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R8,&OP2L  
    mov         R9,&OP2H 
    sub         #8,R12
    sub         #20,R14
    sub         #8,R13
    mov         @R12+,36(R1)
    mov         @R12+,38(R1)
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3
    
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         R4,&OP2L  
    mov         R5,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         R6,&OP2L  
    mov         R7,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         @R13+,R8
    mov         R8,&OP2L  
    mov         @R13+,R9
    mov         R9,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R10,&OP2L  
    mov         @R13,&OP2H 
    sub         #8,R12
    sub         #16,R14
    sub         #4,R13
    mov         @R12+,40(R1)
    mov         @R12+,42(R1)
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3
    
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         R4,&OP2L  
    mov         R5,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         R6,&OP2L  
    mov         R7,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R8,&OP2L  
    mov         R9,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         @R13+,&OP2L  
    mov         @R13+,&OP2H 
    sub         #8,R12
    sub         #12,R14
    mov         @R12+,44(R1)
    mov         @R12+,46(R1)
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3
    
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         R4,&OP2L  
    mov         R5,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         R6,&OP2L  
    mov         R7,&OP2H 
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    add         @R11,R15
    mov         R8,&OP2L  
    mov         R9,&OP2H 
    sub         #8,R12
    sub         #8,R14
    mov         @R12+,48(R1)
    mov         @R12+,R10
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3
    
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         R4,&OP2L  
    mov         R5,&OP2H  
    mov         @R14+,&MAC32L
    mov         @R14+,&MAC32H
    mov         @R11,R15
    mov         R6,&OP2L  
    mov         R7,&OP2H 
    sub         #8,R12
    mov         @R12+,R8
    mov         @R12+,R9
    mov         @R12+,&RES0
    mov         @R12+,&RES1
    add         @R11,R15
    mov         R15,&RES2
    clr         &RES3

    mov         R4,&OP2L  
    mov         R5,&OP2H 
    sub         #8,R12
    mov         @R12+,R6
    mov         @R12+,R7
    mov         @R12+,R4
    mov         @R12+,R5
    
    
    
    ;;;;;;;;;;;;;;;;;;;;;;;
    ; Reduction
    ;;;;;;;;;;;;;;;;;;;;;;;
    
;    mov         98(R1),R15
    mov         82(R1),R15

    sub         #8,R12
    mov         #RES1,R14
    mov         #RES2,R13
          
    mov         R1,R11
    add         #32,R11
          
    clr         &RES2
    clr         &RES3
          
    mov         #38,&MAC32L
    mov         #0,&MAC32H
          
    mov         @R1+,&RES0
    mov         @R1+,&RES1
    mov         @R11+,&OP2L
    mov         @R11+,&OP2H
    mov         @R12,0(R15)
    mov         @R14,2(R15)
    mov         @R13,&RES0
    clr         &RES1
    clr         &RES2
    
    add         @R1+,&RES0
    addc        @R1+,&RES1
    mov         @R11+,&OP2L
    mov         @R11+,&OP2H
    mov         @R12,4(R15)
    mov         @R14,6(R15)
    mov         @R13,&RES0
    clr         &RES1
    clr         &RES2
    
    add         @R1+,&RES0
    addc        @R1+,&RES1
    mov         @R11+,&OP2L
    mov         @R11+,&OP2H
    mov         @R12,8(R15)
    mov         @R14,10(R15)
    mov         @R13,&RES0
    clr         &RES1
    clr         &RES2
          
    add         @R1+,&RES0
    addc        @R1+,&RES1
    mov         @R11+,&OP2L
    mov         @R11+,&OP2H
    mov         @R12,12(R15)
    mov         @R14,14(R15)
    mov         @R13,&RES0
    clr         &RES1
    clr         &RES2
          
    add         @R1+,&RES0
    addc        @R1+,&RES1
    mov         @R11+,&OP2L
    mov         R10,&OP2H
    mov         @R12,16(R15)
    mov         @R14,R10
    mov         @R13,&RES0
    clr         &RES1
    clr         &RES2
    
    add         @R1+,&RES0
    addc        @R1+,&RES1
    mov         R8,&OP2L
    mov         R9,&OP2H
    mov         @R12,R8
    mov         @R14,R9
    mov         @R13,&RES0
    clr         &RES1
    clr         &RES2
    
    add         @R1+,&RES0
    addc        @R1+,&RES1
    mov         R6,&OP2L
    mov         R7,&OP2H
    mov         @R12,R6
    mov         @R14,R7
    mov         @R13,&RES0
    clr         &RES1
    clr         &RES2
    
    add         @R1+,&RES0
    addc        @R1+,&RES1
    mov         R4,&OP2L
    mov         R5,&OP2H
    mov         @R12,R4
    mov         @R14,R5
    mov         @R13,&RES0
    
    mov         #38,&MPY32L
    mov         @R13,&OP2L
    
    add         @R12,0(R15)
    adc         2(R15)
    adc         4(R15)
    adc         6(R15)
    adc         8(R15)
    adc         10(R15)
    adc         12(R15)
    adc         14(R15)
    adc         16(R15)
    adc         R10
    adc         R8
    adc         R9
    adc         R6
    adc         R7
    adc         R4
    adc         R5
    
    mov         R10,18(R15)
    mov         R8,20(R15)
    mov         R9,22(R15)
    mov         R6,24(R15)
    mov         R7,26(R15)
    
    clr         R11
    adc         R11
    mov         R11,&OP2L
    mov         R4,28(R15)
    mov         R5,30(R15)
    add         @R12,0(R15)
    
    add         #50,R1
    
    ;popm.w      #9,R15

    pop.w       R15
    popm.w      #8,R11
    eint
    
    ret
