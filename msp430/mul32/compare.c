/*
 * File:    compare.c
 * Author:  Gesine Hinterwälder
 * Public Domain
 */

#include "fe.h"

/*
 * if (h != f) return 1
 * else return 0
 */


int compare(unsigned char* h,unsigned char* f)
{
  if((h[0] != f[0]) || (h[1] != f[1]) || (h[2] != f[2]) || (h[3] != f[3]) || (h[4] != f[4]) || (h[5] != f[5]) || (h[6] != f[6]) || (h[7] != f[7]) || (h[8] != f[8]) || (h[9] != f[9]))
    return 1;
  if((h[10] != f[10]) || (h[11] != f[11]) || (h[12] != f[12]) || (h[13] != f[13]) || (h[14] != f[14]) || (h[15] != f[15]) || (h[16] != f[16]) || (h[17] != f[17]) || (h[18] != f[18]) || (h[19] != f[19]))
    return 1;
  if((h[20] != f[20]) || (h[21] != f[21]) || (h[22] != f[22]) || (h[23] != f[23]) || (h[24] != f[24]) || (h[25] != f[25]) || (h[26] != f[26]) || (h[27] != f[27]) || (h[28] != f[28]) || (h[29] != f[29]))
    return 1;
  if((h[30] != f[30]) || (h[31] != f[31]))
    return 1;
  
  return 0;
    
}
