/*
 * File:    fe.h
 * Author:  Gesine Hinterwälder
 * Public Domain
 */

#include "msp430fr5969.h"

typedef unsigned int fe[16];


extern void fe_0(fe h);

extern void fe_1(fe h);

extern void fe_add(fe h,fe f,fe g);
 
extern void fe_sub(fe h,fe f,fe g);

extern void fe_copy(fe h,fe f);

extern void fe_cswap(fe f,fe g,unsigned int b);

extern void fe_mul(fe h,fe f,fe g);

extern void fe_mul121666(fe h,fe f);

extern unsigned int fe_sub_25519(fe out, fe in1);

extern void fe_sq(fe h,fe f);

void fe_tobytes(unsigned char *s,fe h);

void fe_invert(fe out,fe z);

void fe_frombytes(fe h,const unsigned char *s);


int compare(unsigned char* h,unsigned char* f);


void Curve25519_double_point(unsigned char *x2, unsigned char *z2, unsigned char *x, unsigned char *z);

void Curve25519_add_points(unsigned char *x3, unsigned char *z3, unsigned char *x, unsigned char *z, unsigned char *xprime, unsigned char *zprime, unsigned char *x1, unsigned char *z1);

int Curve25519_scalarmult(unsigned char *q,  unsigned char *n,  unsigned char *p);
