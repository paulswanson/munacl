/*
 * File:    fe_tobytes.c
 * Author:  Gesine Hinterwälder
 * Public Domain
 */

#include "fe.h"


void fe_cmov(fe r, fe x, unsigned int mask)
{
  
    mask ^= 0xffff;

    r[0] ^= mask & (x[0] ^ r[0]);
    r[1] ^= mask & (x[1] ^ r[1]);
    r[2] ^= mask & (x[2] ^ r[2]);
    r[3] ^= mask & (x[3] ^ r[3]);
    r[4] ^= mask & (x[4] ^ r[4]);
    r[5] ^= mask & (x[5] ^ r[5]);
    r[6] ^= mask & (x[6] ^ r[6]);
    r[7] ^= mask & (x[7] ^ r[7]);
    r[8] ^= mask & (x[8] ^ r[8]);
    r[9] ^= mask & (x[9] ^ r[9]);
    r[10] ^= mask & (x[10] ^ r[10]);
    r[11] ^= mask & (x[11] ^ r[11]);
    r[12] ^= mask & (x[12] ^ r[12]);
    r[13] ^= mask & (x[13] ^ r[13]);
    r[14] ^= mask & (x[14] ^ r[14]);
    r[15] ^= mask & (x[15] ^ r[15]);
    
}

void fe_tobytes(unsigned char *s, fe h)
{
    unsigned int c;
    fe rt;
    
    c = fe_sub_25519(rt, h);
    fe_cmov(h, rt, c);
    c = fe_sub_25519(rt, h);
    fe_cmov(h, rt, c);
    
    s[0]  = h[0];
    s[1]  = h[0]>>8;
    s[2]  = h[1];
    s[3]  = h[1]>>8;
    s[4]  = h[2];
    s[5]  = h[2]>>8;
    s[6]  = h[3];
    s[7]  = h[3]>>8;
    s[8]  = h[4];
    s[9]  = h[4]>>8;
    s[10] = h[5];
    s[11] = h[5]>>8;
    s[12] = h[6];
    s[13] = h[6]>>8;
    s[14] = h[7];
    s[15] = h[7]>>8;
    s[16] = h[8];
    s[17] = h[8]>>8;
    s[18] = h[9];
    s[19] = h[9]>>8;
    s[20] = h[10];
    s[21] = h[10]>>8;
    s[22] = h[11];
    s[23] = h[11]>>8;
    s[24] = h[12];
    s[25] = h[12]>>8;
    s[26] = h[13];
    s[27] = h[13]>>8;
    s[28] = h[14];
    s[29] = h[14]>>8;
    s[30] = h[15];
    s[31] = h[15]>>8;
}
