/*
 * File:    curve25519.c
 * Author:  Gesine Hinterwälder
 * Public Domain
 */

#include "api.h"
#include "fe.h"

int crypto_scalarmult_curve25519(unsigned char *q,  const unsigned char *n,  const unsigned char *p)
{
  signed int i;
  unsigned char e[32], swap = 0, b;
  
  fe x1;
  fe x2;
  fe z2;
  fe x3;
  fe z3;
  fe tmp0;
  fe tmp1;

  for(i=0;i<32;i++) e[i] = n[i];

  e[0] &= 248;
  e[31] &= 127;
  e[31] |= 64; 

  fe_frombytes(x1,p);
  fe_1(x2);
  fe_0(z2);
  fe_1(z3);
  fe_copy(x3,x1);

  for (i = 254;i >= 0;--i) {

    b = (e[i / 8] >> (i & 7)) &1;
    swap ^= b;
    fe_cswap(x2,x3,swap);
    fe_cswap(z2,z3,swap);
    swap = b;

    fe_sub(tmp0,x3,z3);
    fe_sub(tmp1,x2,z2);
    fe_add(x2,x2,z2);
    fe_add(z2,x3,z3);
    fe_mul(z3,tmp0,x2);
    fe_mul(z2,z2,tmp1);
    fe_sq(tmp0,tmp1);
    fe_sq(tmp1,x2);
    fe_add(x3,z3,z2);
    fe_sub(z2,z3,z2);
    fe_mul(x2,tmp1,tmp0);
    fe_sub(tmp1,tmp1,tmp0);
    fe_sq(z2,z2);
    fe_mul121666(z3,tmp1);
    fe_sq(x3,x3);
    fe_add(tmp0,tmp0,z3);
    fe_mul(z3,x1,z2);
    fe_mul(z2,tmp1,tmp0);

  }

  fe_cswap(x2,x3,swap);
  fe_cswap(z2,z3,swap);

  // fe_26limbs_invert(z2,z2);
  //fe t0 - > x1;
  //fe t1 -> x3;
  //fe t2 -> z3;
  //fe t3 -> tmp0;

  fe_sq(x1,z2); 

  fe_sq(x3,x1); 
  fe_sq(x3,x3);

  fe_mul(x3,z2,x3);

  fe_mul(x1,x1,x3);

  fe_sq(z3,x1); 

  fe_mul(x3,x3,z3);

  fe_sq(z3,x3); 
  //        for (i = 1;i < 5;++i) fe_sq(z3,z3);
  fe_sq(z3,z3);
  fe_sq(z3,z3);
  fe_sq(z3,z3);
  fe_sq(z3,z3);

  fe_mul(x3,z3,x3);

  fe_sq(z3,x3); 
  for (i = 1;i < 10;++i) fe_sq(z3,z3);

  fe_mul(z3,z3,x3);

  fe_sq(tmp0,z3); 
  for (i = 1;i < 20;++i) fe_sq(tmp0,tmp0);

  fe_mul(z3,tmp0,z3);

  fe_sq(z3,z3); 
  for (i = 1;i < 10;++i) fe_sq(z3,z3);

  fe_mul(x3,z3,x3);

  fe_sq(z3,x3); 
  for (i = 1;i < 50;++i) fe_sq(z3,z3);

  fe_mul(z3,z3,x3);

  fe_sq(tmp0,z3); 
  for (i = 1;i < 100;++i) fe_sq(tmp0,tmp0);

  fe_mul(z3,tmp0,z3);

  fe_sq(z3,z3); 
  
  for (i = 1;i < 50;++i) fe_sq(z3,z3);

  fe_mul(x3,z3,x3);

  fe_sq(x3,x3); 
  for (i = 1;i < 5;++i) fe_sq(x3,x3);

  fe_mul(z2,x3,x1);

  // end inversion

  fe_mul(x2,x2,z2);
  fe_tobytes(q,x2);

  return 0;
}

static const unsigned char base[32] = {9};

int crypto_scalarmult_curve25519_base(
    unsigned char *q, 
    const unsigned char *n
    )
{
  return crypto_scalarmult_curve25519(q,n,base);
}
