#include <msp430fr5969.h>
#include <legacymsp430.h>

#include "print.h"

static unsigned long long ctr = 0;
static int isinit = 0;

static void
__attribute__((__interrupt__(TIMER0_A1_VECTOR)))
  ctrinc (void)
{
  ctr += 0x10000ULL;
  TA0IV = 0;
}

static void init_ctra()
{
  TA0CTL = TACLR;
  TA0CTL = TASSEL_2 + MC_2 + TAIE + ID_3;
  eint();
  isinit = 1;
}

unsigned long long cpucycles()
{
  if(!isinit) init_ctra();
  return (ctr | TA0R) << 3;
}
