#include <stdlib.h>
#include <stdio.h>
#include "../fe.h"
#include "print.h"
#include "fail.h"
#include "randombytes.h"

#define NTESTS 20

static const unsigned char badn[32] = {0x56,0x2c,0x1e,0xb5,0xfd,0xb2,0x81,0x29,
                                       0xbd,0x37,0x49,0x58,0x35,0xd4,0xb1,0x30,
                                       0x7d,0xdb,0x57,0x38,0x80,0x12,0x17,0x42,
                                       0xf7,0x13,0xf1,0x05,0x67,0x69,0xd5,0xbf};

void fe_print(const fe x)
{
  int i;
  char str[6];
  print("(");
  for(i=15;i>0;i--)
  {
    sprintf(str,"%u",x[i]);
    print(str);
    print("* 2^");
    sprintf(str,"%u",16*i);
    print(str);
    print(" + ");
  }
  sprintf(str,"%u",x[i]);
  print(str);
  print("* 2^");
  sprintf(str,"%u",16*i);
  print(str);
  print(")");
}

int main(void)
{
  fe a,b,r;
  int i;

  for(i=0;i<NTESTS;i++)
  {

    randombytes((unsigned char *)a,32);
    randombytes((unsigned char *)b,32);

    fe_add(r,a,b);

    print("(");
    fe_print(a);
    print(" + ");
    fe_print(b);
    print(" - ");
    fe_print(r);
    print(") % (2^255-19)\n");

    fe_sub(r,a,b);

    print("(");
    fe_print(a);
    print(" - ");
    fe_print(b);
    print(" - ");
    fe_print(r);
    print(") % (2^255-19)\n");

    print("(");
    fe_print(a);
    print(" * ");
    fe_print(b);
    fe_mul(r,a,b);
    print(" - ");
    fe_print(r);
    print(") % (2^255-19)\n");

    print("(");
    fe_print(a);
    print(" * ");
    fe_print(a);
    fe_sq(r,a);
    print(" - ");
    fe_print(r);
    print(") % (2^255-19)\n");
  }

  serial_write(4);

  return 0;
}
