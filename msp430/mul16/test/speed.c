#include <stdlib.h>
#include <msp430fr5969.h>
#include "../api.h"
#include "print.h"
#include "cpucycles.h"
#include "fail.h"

#define NRUNS 5

#define nlen crypto_scalarmult_SCALARBYTES
#define plen crypto_scalarmult_BYTES

static unsigned char n[nlen];
static unsigned char p[plen];

int main(void)
{
  unsigned int i;
  unsigned long long t[NRUNS];


  /* UNCOMMENT TO BENCHMARK AT 16MHz 
  
  FRCTL0 = 0xA500 | ((1) << 4);                          
  CSCTL0_H = 0xa5;                                       
  CSCTL1 = DCOFSEL_4 | DCORSEL;                          
  CSCTL2 = SELA__VLOCLK | SELS__DCOCLK | SELM__DCOCLK;   
  CSCTL3 = DIVA__1 | DIVS__1 | DIVM__1;                  
  CSCTL0_H = 0;

  */

  print("\n");

  for (i = 0;i < nlen;++i) n[i] = i + 1;
  for (i = 0;i < plen;++i) p[i] = i + 2;

  for(i=0;i<NRUNS;i++)
  {
    t[i] = cpucycles();
    crypto_scalarmult_base(p,n);
  }
  print_speed("crypto_scalarmult_base",-1,t,NRUNS);

  for(i=0;i<NRUNS;i++)
  {
    t[i] = cpucycles();
    crypto_scalarmult(p,n,p);
  }
  print_speed("crypto_scalarmult",-1,t,NRUNS);

  serial_write(4);
  return 0;
}
