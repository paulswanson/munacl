#ifndef PRINT_H
#define PRINT_H

void print(const char *s);

void serial_write(char c);

void printllu(unsigned long long x);

void print_speed(const char *primitive, const unsigned int bytes, const unsigned long long *t, unsigned int tlen);

void print_stack(const char *primitive, const unsigned int bytes, unsigned int stack);

#endif
