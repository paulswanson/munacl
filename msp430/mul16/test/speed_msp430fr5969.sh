#!/bin/sh

DEVICE=/dev/ttyACM1
LIBMSP430_PATH=~/src/MSPDebugStack_OS_Package/
MYDIR=`dirname $0`

export LD_LIBRARY_PATH=$LIBMSP430_PATH
stty -F $DEVICE raw icanon eof \^d 9600

mspdebug tilib "prog $MYDIR/speed " 2>&1 > /dev/null
cat < $DEVICE
