#include <stdlib.h>
#include "../api.h"
#include "print.h"
#include "fail.h"
#include "randombytes.h"

#define nlen crypto_scalarmult_SCALARBYTES
#define qlen crypto_scalarmult_BYTES

#define MAXSTACK 1500

unsigned char i;
unsigned char n[nlen];
unsigned char q[qlen];

unsigned int ctr=0,newctr;
unsigned char canary;
volatile unsigned char *p;
extern unsigned char _end; 

static unsigned int stack_count(unsigned char canary,volatile unsigned char *a)
{
  volatile unsigned char *p = (a-MAXSTACK);
  unsigned int c = 0;
  while(*p == canary && p < a)
  {
    p++;
    c++;
  }
  return c;
} 

#define WRITE_CANARY(X) {p=X;while(p>= (X-MAXSTACK)) *(p--) = canary;}
 
int main(void)
{
  volatile unsigned char a; /* Mark the beginning of the stack */

  print("\n");

  for(i=0;i<5;i++)
  {
    randombytes(&canary,1);
    WRITE_CANARY(&a);
    crypto_scalarmult(q,n,q);
    newctr = MAXSTACK - stack_count(canary,&a);
    ctr = (newctr>ctr)?newctr:ctr;
  }
  print_stack("crypto_scalarmult",-1,ctr);

  for(i=0;i<5;i++)
  {
    randombytes(&canary,1);
    WRITE_CANARY(&a);
    crypto_scalarmult_base(q,n);
    newctr = MAXSTACK - stack_count(canary,&a);
    ctr = (newctr>ctr)?newctr:ctr;
  }
  print_stack("crypto_scalarmult_base",-1,ctr);

  serial_write(4);
  return 0;
}
