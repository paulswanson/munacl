#include "msp430fr5969.h"

static unsigned char serial_isinit = 0;

static void init_serial()
{
  // Stop watchdog timer to prevent time out reset
  WDTCTL = WDTPW + WDTHOLD;

  // Configure GPIO
  P2SEL1 |= BIT0 | BIT1;                    // USCI_A0 UART operation
  P2SEL0 &= ~(BIT0 | BIT1);

  // Disable the GPIO power-on default high-impedance mode to activate
  // previously configured port settings
  PM5CTL0 &= ~LOCKLPM5;

  // Set clock to 8 MHz 
  CSCTL0_H = 0xa5;                                            // Unlock clock registers
  CSCTL1 = DCOFSEL_3 | DCORSEL;                               // Set DCO to 8MHz        
  CSCTL2 = SELA__VLOCLK | SELS__DCOCLK | SELM__DCOCLK;        // select DCO for master clock
  CSCTL3 = DIVA__1 | DIVS__1 | DIVM__1;                       // set all dividers to one
  CSCTL0_H = 0;    
  // Lock CS registers

  // Configure USCI_A0 for UART mode 8 MHz
  UCA0CTLW0 = UCSWRST;                      // Put eUSCI in reset
  UCA0CTLW0 |= UCSSEL__SMCLK;               // CLK = SMCLK
  UCA0BR0 = 52;                             
  UCA0BR1 = 0x00;
  UCA0MCTLW |= UCOS16 | UCBRF_1;
  UCA0CTLW0 &= ~UCSWRST;                    // Initialize eUSCI
//  UCA0IE |= UCRXIE;                         // Enable USCI_A0 RX interrupt

  serial_isinit = 1;
}

void serial_write(char c)
{
  if(!serial_isinit)
    init_serial();
  while(!(UCA0IFG&UCTXIFG));
  UCA0TXBUF = c;
}

void print(const char *s)
{
  while(*s != 0)
    serial_write(*s++);
}

void printllu(unsigned long long x)
{
  char str[24];
  int i = 22;
  str[23]=0;
  if(x==0)
    print("0");
  while(x>0)
  {
    str[i] = (char)((x%10)+48);
    i--;
    x = x/10;
  }
  print(str+i+1);
}


void print_speed(const char *primitive, const unsigned int bytes, const unsigned long long *t, unsigned int tlen)
{
  unsigned int i;
  print(primitive);
  print(": ");
  if(bytes != (unsigned int)-1)
  {
    print("[");
    printllu(bytes);
    print("] ");
  }

  for(i=0;i<tlen-1;i++)
  {
    printllu(t[i+1]-t[i]);
    print(" ");
  }
  print("\n");
}


void print_stack(const char *primitive, const unsigned int bytes, unsigned int stack)
{
  print(primitive);
  print(": ");
  if(bytes != (unsigned int)-1)
  {
    print("[");
    printllu(bytes);
    print("] ");
  }

  printllu(stack);
    print(" stack bytes\n");
}
