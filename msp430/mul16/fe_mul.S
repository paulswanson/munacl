/*
 * File:    fe_mul.S
 * Author:  Gesine Hinterwälder
 *          modifications for gcc by Peter Schwabe
 * Public Domain
 */

#include "msp430fr5969.h" 

.global fe_mul
.type fe_mul, @function


#ifdef TACCTL2_
  #define CCTLX TACCTL2
  #define CCRX TACCR2
#else
  #define CCTLX TACCTL0
  #define CCRX TACCR0
#endif
      

fe_mul:

    dint
    nop
    pushm.w     #8,R11
    push.w      R15
    sub         #96,R1
    
    mov         #RESLO,R12
      
    ; R14 holds f
    ; R13 holds g
    ; R1 (0 ... 30)   hold L = A_l * B_l
    ; R1 (32 ... 62)  hold H = A_h * B_h
    ; R1 (64 ... 78)  hold A_l - A_h
    ; R1 (80 ... 94)  hold B_l - B_h
    ; R1 (64 ... 94)  hold -M+L+H
  
; Computation of L = A_l * B_l

    mov         @R13+,R4
    mov         @R13+,R5
    mov         @R13+,R6
    mov         @R13+,R7
    mov         @R13+,R8
    
    mov         @R14+,R11
    
    mov         R11,&MPY
    mov         R4,&OP2
    mov         @R13+,R9
    mov         @R13+,R10
    mov         @R12+,0(R1)
    mov         @R12+,&RESLO
    clr         &RESHI
        
    mov         R11,&MAC
    mov         R5,&OP2
    mov         @R14,&MAC
    mov         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    mov         @R12+,2(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R11,&MAC
    mov         R6,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #4,R14
    mov         @R12+,4(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R11,&MAC
    mov         R7,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #6,R14
    mov         @R12+,6(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R11,&MAC
    mov         R8,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #8,R14
    mov         @R12+,8(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R11,&MAC
    mov         R9,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #10,R14
    mov         @R12+,10(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R11,&MAC
    mov         R10,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #12,R14
    mov         @R12+,12(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R11,&MAC
    mov         @R13,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,R11
    mov         R11,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #14,R14
    mov         @R12+,14(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         R11,&MAC
    add         @R12,R15
    mov         R5,&OP2
    sub         #4,R12
    sub         #10,R14
    mov         @R12+,16(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         R11,&MAC
    add         @R12,R15
    mov         R6,&OP2
    sub         #4,R12
    sub         #8,R14
    mov         @R12+,18(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         R11,&MAC
    add         @R12,R15
    mov         R7,&OP2
    sub         #4,R12
    sub         #6,R14
    mov         @R12+,20(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         R11,&MAC
    add         @R12,R15
    mov         R8,&OP2
    sub         #4,R12
    sub         #4,R14
    mov         @R12+,22(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13,&OP2
    mov         @R14,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         R11,&MAC
    add         @R12,R15
    mov         R9,&OP2
    sub         #4,R12
    mov         @R12+,24(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13,&OP2
    mov         R11,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    sub         #4,R12
    mov         @R12+,26(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13+,&OP2
    sub         #4,R12
    mov         @R12+,28(R1)
    mov         @R12+,30(R1)

; Computation of H = A_h * B_h

    mov         @R13+,R4
    mov         @R13+,R5
    mov         @R13+,R6
    mov         @R13+,R7
    mov         @R13+,R8
    
    mov         @R14+,R11
    
    mov         R11,&MPY
    mov         R4,&OP2
    mov         @R13+,R9
    mov         @R13+,R10
    sub         #4,R12
    mov         @R12+,32(R1)
    mov         @R12+,&RESLO
    clr         &RESHI
        
    mov         R11,&MAC
    mov         R5,&OP2
    mov         @R14,&MAC
    mov         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    mov         @R12+,34(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R11,&MAC
    mov         R6,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #4,R14
    mov         @R12+,36(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R11,&MAC
    mov         R7,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #6,R14
    mov         @R12+,38(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R11,&MAC
    mov         R8,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #8,R14
    mov         @R12+,40(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R11,&MAC
    mov         R9,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #10,R14
    mov         @R12+,42(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R11,&MAC
    mov         R10,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #12,R14
    mov         @R12+,44(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R11,&MAC
    mov         @R13,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,R11
    mov         R11,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #14,R14
    mov         @R12+,46(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         R11,&MAC
    add         @R12,R15
    mov         R5,&OP2
    sub         #4,R12
    sub         #10,R14
    mov         @R12+,48(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         R11,&MAC
    add         @R12,R15
    mov         R6,&OP2
    sub         #4,R12
    sub         #8,R14
    mov         @R12+,50(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         R11,&MAC
    add         @R12,R15
    mov         R7,&OP2
    sub         #4,R12
    sub         #6,R14
    mov         @R12+,52(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         R11,&MAC
    add         @R12,R15
    mov         R8,&OP2
    sub         #4,R12
    sub         #4,R14
    mov         @R12+,54(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13,&OP2
    mov         @R14,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         R11,&MAC
    add         @R12,R15
    mov         R9,&OP2
    sub         #4,R12
    mov         @R12+,56(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13,&OP2
    mov         R11,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    sub         #4,R12
    mov         @R12+,58(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         @R13+,&OP2
    sub         #4,R12
    mov         @R12+,60(R1)
    mov         @R12+,62(R1)
    

; Set R14 and R13 to original value

    sub         #32,R14
    sub         #32,R13

; Computation of |A_l - A_h|

    clr         R15
         
    mov         @R14+,R4
    mov         @R14+,R5
    mov         @R14+,R6
    mov         @R14+,R7
    mov         @R14+,R8
    mov         @R14+,R9
    mov         @R14+,R10
    mov         @R14+,R11
         
    sub         @R14+,R4
    subc        @R14+,R5
    subc        @R14+,R6
    subc        @R14+,R7
    subc        @R14+,R8
    subc        @R14+,R9
    subc        @R14+,R10
    subc        @R14+,R11
    
    sbc         R15
    push.w      R15
        
    xor         R15,R4
    xor         R15,R5
    xor         R15,R6
    xor         R15,R7
    xor         R15,R8
    xor         R15,R9
    xor         R15,R10
    xor         R15,R11
         
    and         #1,R15
         
    add         R15,R4
    adc         R5
    adc         R6
    adc         R7
    adc         R8
    adc         R9
    adc         R10
    adc         R11
         
    mov         R4,82(R1)
    mov         R5,84(R1)
    mov         R6,86(R1)
    mov         R7,88(R1)
    mov         R8,90(R1)
    mov         R9,92(R1)
    mov         R10,94(R1)
    mov         R11,96(R1)
    
; Computation of |B_l - B_h|

    clr         R15
         
    mov         @R13+,R4
    mov         @R13+,R5
    mov         @R13+,R6
    mov         @R13+,R7
    mov         @R13+,R8
    mov         @R13+,R9
    mov         @R13+,R10
    mov         @R13+,R11

    sub         @R13+,R4
    subc        @R13+,R5
    subc        @R13+,R6
    subc        @R13+,R7
    subc        @R13+,R8
    subc        @R13+,R9
    subc        @R13+,R10
    subc        @R13+,R11
    
    sbc         R15
    xor         R15,0(R1)
         
    xor         R15,R4
    xor         R15,R5
    xor         R15,R6
    xor         R15,R7
    xor         R15,R8
    xor         R15,R9
    xor         R15,R10
    xor         R15,R11
         
    and         #1,R15
         
    add         R15,R4
    adc         R5
    adc         R6
    adc         R7
    adc         R8
    adc         R9
    adc         R10
    adc         R11

// Computation of M

    mov         R1,R14
    add         #82,R14
    
    mov         @R14+,R13
    
    mov         R13,&MPY
    mov         R4,&OP2
    sub         #4,R12
    mov         @R12+,66(R1)
    mov         @R12+,&RESLO
    clr         &RESHI
        
    mov         R13,&MAC
    mov         R5,&OP2
    mov         @R14,&MAC
    mov         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    mov         @R12+,68(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R13,&MAC
    mov         R6,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #4,R14
    mov         @R12+,70(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R13,&MAC
    mov         R7,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #6,R14
    mov         @R12+,72(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R13,&MAC
    mov         R8,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #8,R14
    mov         @R12+,74(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R13,&MAC
    mov         R9,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #10,R14
    mov         @R12+,76(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R13,&MAC
    mov         R10,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #12,R14
    mov         @R12+,78(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         R13,&MAC
    mov         R11,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R5,&OP2
    mov         @R14+,R13
    mov         R13,&MAC
    add         @R12,R15
    mov         R4,&OP2
    sub         #4,R12
    sub         #14,R14
    mov         @R12+,80(R1)
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         R11,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R6,&OP2
    mov         R13,&MAC
    add         @R12,R15
    mov         R5,&OP2
    sub         #4,R12
    sub         #10,R14
    mov         @R12+,R4
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         R11,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R7,&OP2
    mov         R13,&MAC
    add         @R12,R15
    mov         R6,&OP2
    sub         #4,R12
    sub         #8,R14
    mov         @R12+,R5
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         R11,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R8,&OP2
    mov         R13,&MAC
    add         @R12,R15
    mov         R7,&OP2
    sub         #4,R12
    sub         #6,R14
    mov         @R12+,R6
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         R11,&OP2
    mov         @R14+,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         @R14+,&MAC
    add         @R12,R15
    mov         R9,&OP2
    mov         R13,&MAC
    add         @R12,R15
    mov         R8,&OP2
    sub         #4,R12
    sub         #4,R14
    mov         @R12+,R7
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         R11,&OP2
    mov         @R14,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    mov         R13,&MAC
    add         @R12,R15
    mov         R9,&OP2
    sub         #4,R12
    mov         @R12+,R8
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         R11,&OP2
    mov         R13,&MAC
    mov         @R12,R15
    mov         R10,&OP2
    sub         #4,R12
    mov         @R12+,R9
    mov         @R12+,&RESLO
    add         @R12,R15
    mov         R15,&RESHI
    
    mov         @R14+,&MAC
    mov         R11,&OP2
    sub         #4,R12
    mov         @R12+,R10
    mov         @R12+,R11
    
; Computation of -M_hat
    
    pop.w       R15
    xor         #0xffff,R15
    clr         R13
             
    xor         R15,64(R1)
    xor         R15,66(R1)
    xor         R15,68(R1)
    xor         R15,70(R1)
    xor         R15,72(R1)
    xor         R15,74(R1)
    xor         R15,76(R1)
    xor         R15,78(R1)
    xor         R15,R4
    xor         R15,R5
    xor         R15,R6
    xor         R15,R7
    xor         R15,R8
    xor         R15,R9
    xor         R15,R10
    xor         R15,R11
    xor         R15,R13
    
    
; add contents of 1 & R15 for two's complement and add F0*G0 to M_hat

    mov         R1,R14

    and         #1,R15
    clrc
    bis         R15,R2
        
    addc        @R14+,64(R1)
    addc        @R14+,66(R1)
    addc        @R14+,68(R1)
    addc        @R14+,70(R1)
    addc        @R14+,72(R1)
    addc        @R14+,74(R1)
    addc        @R14+,76(R1)
    addc        @R14+,78(R1)
    addc        @R14+,R4
    addc        @R14+,R5
    addc        @R14+,R6
    addc        @R14+,R7
    addc        @R14+,R8
    addc        @R14+,R9
    addc        @R14+,R10
    addc        @R14+,R11
    adc         R13

; add F1*G1 to M_hat

    add         @R14+,64(R1)
    addc        @R14+,66(R1)
    addc        @R14+,68(R1)
    addc        @R14+,70(R1)
    addc        @R14+,72(R1)
    addc        @R14+,74(R1)
    addc        @R14+,76(R1)
    addc        @R14+,78(R1)
    addc        @R14+,R4
    addc        @R14+,R5
    addc        @R14+,R6
    addc        @R14+,R7
    addc        @R14+,R8
    addc        @R14+,R9
    addc        @R14+,R10
    addc        @R14+,R11
    adc         R13
    
; add (F0*G0 + F0*G0 - M_hat) to result

    mov         R1,R15
    add         #32,R15

    add          @R14+,16(R1)
    addc         @R14+,18(R1)
    addc         @R14+,20(R1)
    addc         @R14+,22(R1)
    addc         @R14+,24(R1)
    addc         @R14+,26(R1)
    addc         @R14+,28(R1)
    addc         @R14+,30(R1)
    addc         @R15+,R4
    addc         @R15+,R5
    addc         @R15+,R6
    addc         @R15+,R7
    addc         @R15+,R8
    addc         @R15+,R9
    addc         @R15+,R10
    addc         @R15+,R11
    addc         R13,48(R1)
    adc          50(R1)
    adc          52(R1)
    adc          54(R1)
    adc          56(R1)
    adc          58(R1)
    adc          60(R1)
    adc          62(R1)


    ;;;;;;;;;;;;;;;;;;;;;;;
    ; Reduction
    ;;;;;;;;;;;;;;;;;;;;;;;
    
    mov         96(R1),R15
         
    mov         R1,R13
    add         #48,R13
    mov         #RESHI,R12
         
    mov         #38,&MAC
         
    mov         @R1+,&RESLO
    clr         &RESHI
    mov         R4,&OP2
    mov         &RESLO,0(R15)
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         R5,&OP2
    mov         &RESLO,2(R15)
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         R6,&OP2
    mov         &RESLO,4(R15)
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         R7,&OP2
    mov         &RESLO,6(R15)
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         R8,&OP2
    mov         &RESLO,8(R15)
    mov         @R12,&RESLO
    clr         &RESHI
    
    add         @R1+,&RESLO
    adc         &RESHI
    mov         R9,&OP2
    mov         &RESLO,10(R15)
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         R10,&OP2
    mov         &RESLO,12(R15)
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         R11,&OP2
    mov         &RESLO,R14
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         @R13+,&OP2
    mov         &RESLO,R4
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         @R13+,&OP2
    mov         &RESLO,R5
    mov         @R12,&RESLO
    clr         &RESHI
    
    add         @R1+,&RESLO
    adc         &RESHI
    mov         @R13+,&OP2
    mov         &RESLO,R6
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         @R13+,&OP2
    mov         &RESLO,R7
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         @R13+,&OP2
    mov         &RESLO,R8
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         @R13+,&OP2
    mov         &RESLO,R9
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         @R13+,&OP2
    mov         &RESLO,R10
    mov         @R12,&RESLO
    clr         &RESHI
         
    add         @R1+,&RESLO
    adc         &RESHI
    mov         @R13+,&OP2
    mov         &RESLO,R11
         
    mov         #38,&MPY
    mov         @R12,&OP2
    
    clr         R13
    add         &RESLO,0(R15)
    adc         2(R15)
    adc         4(R15)
    adc         6(R15)
    adc         8(R15)
    adc         10(R15)
    adc         12(R15)
    adc         R14
    adc         R4
    adc         R5
    adc         R6
    adc         R7
    adc         R8
    adc         R9
    adc         R10
    adc         R11
    
    mov         R14,14(R15)
    mov         R4,16(R15)
    mov         R5,18(R15)
    mov         R6,20(R15)
    mov         R7,22(R15)
    mov         R8,24(R15)
    mov         R9,26(R15)
    mov         R10,28(R15)
    mov         R11,30(R15)

    adc         R13
    mov         R13,&OP2
    add         &RESLO,0(R15)
         
    add         #64,R1
    
    pop.w       R15
    popm.w      #8,R11
    eint
    
    ret
