/*
 * File:    fe_frombytes.c
 * Author:  Gesine Hinterwälder
 * Public Domain
 */

#include "fe.h"

/*
 * Transform from byte to unsigned radix 2^16 representations
 */


void fe_frombytes(fe h,const unsigned char *s)
{

      h[0] = (s[1] << 8) | s[0];
      h[1] = (s[3] << 8) | s[2];
      h[2] = (s[5] << 8) | s[4];
      h[3] = (s[7] << 8) | s[6];
      h[4] = (s[9] << 8) | s[8];
      h[5] = (s[11] << 8) | s[10];
      h[6] = (s[13] << 8) | s[12];
      h[7] = (s[15] << 8) | s[14];
      h[8] = (s[17] << 8) | s[16];
      h[9] = (s[19] << 8) | s[18];
      h[10] = (s[21] << 8) | s[20];
      h[11] = (s[23] << 8) | s[22];
      h[12] = (s[25] << 8) | s[24];
      h[13] = (s[27] << 8) | s[26];
      h[14] = (s[29] << 8) | s[28];
      h[15] = (s[31] << 8) | s[30];

}
