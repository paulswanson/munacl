/*
 * File:    fe_cswap.S
 * Author:  Gesine Hinterwälder
 *          modifications for gcc by Peter Schwabe
 * Public Domain
 */
 
#include "msp430fr5969.h"

.global fe_cswap
.type fe_cswap, @function

   
    
#ifdef TACCTL2_
  #define CCTLX TACCTL2
  #define CCRX TACCR2
#else
  #define CCTLX TACCTL0
  #define CCRX TACCR0
#endif

    ; if (b = 1) swap f and g
    ; else do nothing
    ; R13 holds b
    ; R15 holds pointer to f
    ; R14 holds pointer to g

fe_cswap:

    pushm.w    #2,R11
    
    clr.w      R11
    sub.w      R13,R11
    
    mov.w      R15,R13
    mov.w      R14,R12
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,0(R15)
    xor.w      R10,0(R14)
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,2(R15)
    xor.w      R10,2(R14)
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,4(R15)
    xor.w      R10,4(R14)  
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,6(R15)
    xor.w      R10,6(R14)
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,8(R15)
    xor.w      R10,8(R14)
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,10(R15)
    xor.w      R10,10(R14)
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,12(R15)
    xor.w      R10,12(R14)
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,14(R15)
    xor.w      R10,14(R14)  
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,16(R15)
    xor.w      R10,16(R14)
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,18(R15)
    xor.w      R10,18(R14)
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,20(R15)
    xor.w      R10,20(R14)
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,22(R15)
    xor.w      R10,22(R14)
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,24(R15)
    xor.w      R10,24(R14)  
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,26(R15)
    xor.w      R10,26(R14)
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,28(R15)
    xor.w      R10,28(R14)
               
    mov.w      @R13+,R10
    xor.w      @R12+,R10
    and.w      R11,R10
    xor.w      R10,30(R15)
    xor.w      R10,30(R14)
      
  
    popm.w    #2,R11

    ret
